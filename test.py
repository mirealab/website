import sys
import locale 
def calculate(zeros,s,temp):
     if temp=='mm':
          s=(s*10**zeros)/10
     elif temp=='cm':
          s=(s*10**zeros)/100
     elif temp=='dm':
          s=(s*10**zeros)/1000
     elif temp=='m':
          s=(s*10**zeros)/10000
     elif temp=='km':
          s=(s*10**zeros)/100000000
     return s
def recalculate(temp):
     sum=1
     if temp=='mm':
          sum=1
     elif temp=='cm':
          sum+=1
     elif temp=='dm':
          sum+=2
     elif temp=='m':
          sum+=4
     elif temp=='km':
          sum+=6
     return sum


if (len(sys.argv)>1):
    s=float(sys.argv[1])
    fi=sys.argv[2]
    se=sys.argv[3]

rez="nothing"
zeros = recalculate(fi);
rez = calculate(zeros,s,se);
rez =locale.format("%f", rez, 1);

print(s," ",fi," to ",se," = ",rez)
